# README #

## Author

Prashant Soni

### Description? ###
This project is build on cucumber JVM framework using rest assured. 

* I have setup method in runner class to set base url and base path.
* Feature file holds scenarios.
* Step definitions has code to complete the tests.
* Api session will log out at the end of suite run.

## Pre-requisite
* java 11
* Maven 

### How to run tests ###

* mvn clean test 


package com.currencycloud.test;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import io.restassured.RestAssured;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import static com.currencycloud.helpers.CucumberReports.generateReports;
import static com.currencycloud.steps.TokenSteps.iLogOutTheSystem;

@CucumberOptions(features = "src/test/resources/features",
        plugin = {"pretty", "json:target/cucumber-parallel/cucumber.json"},
        glue = {"com.currencycloud.steps"},
        tags = "@quote")

public class RunCucumberTest extends AbstractTestNGCucumberTests {
    @BeforeSuite
    public static void setUp() {
        RestAssured.baseURI = "https://devapi.currencycloud.com";
        RestAssured.basePath = "/v2";
    }

    @AfterSuite
    public static void reports() {
        iLogOutTheSystem(); // Ends the Api Session
        generateReports(); // Generates reports in target folder
    }
}

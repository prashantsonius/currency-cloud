package com.currencycloud.steps;

import com.currencycloud.helpers.CommonActions;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import java.io.IOException;
import static io.restassured.RestAssured.given;

public class TokenSteps extends CommonActions {

    @Given("I set login credential")
    public void iSetLoginCredential() throws IOException {
        loadProperties(prop);
        request = given()
                .header("Content-Type", "multipart/form-data")
                .multiPart("login_id", prop.getProperty("LOGIN_ID"))
                .multiPart("api_key", prop.getProperty("API_KEY"));
    }

    @When("I make call to get auth token")
    public void iMakeCallToGetAuthToken() {
        response = request.when().post("/authenticate/api");
    }

    @Then("I get auth token")
    public void iGetAuthToken() {
        authToken = response.body().jsonPath().getString("auth_token");
    }

    @Then("I check status code {string}")
    public void iCheckStatusCode(String statusCode) {
        response.then().log().ifError().statusCode(Integer.parseInt(statusCode));
    }

    public static void iLogOutTheSystem() {
        response = request.when().post("/authenticate/close_session");
        Assert.assertEquals(response.statusCode(), 200);
    }
}

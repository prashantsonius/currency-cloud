package com.currencycloud.steps;

import com.currencycloud.helpers.CommonActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.io.IOException;
import java.util.HashMap;

import static io.restassured.RestAssured.given;

public class QuoteSptes extends CommonActions {

    @Then("I set {string} {string} {string} to {string}")
    public void iSetTo(String fixedSide, String sellAmount, String sellCurrency, String buyCurrency) throws IOException {
        amount = sellAmount;
        loadProperties(prop);
        HashMap<String, String> paramsMap = new HashMap<String, String>();
        paramsMap.put("buy_currency", buyCurrency);
        paramsMap.put("sell_currency", sellCurrency);
        paramsMap.put("fixed_side", fixedSide);
        paramsMap.put("amount", sellAmount);
        request = given()
                .header("X-Auth-Token", authToken)
                .params(paramsMap);
    }

    @When("I make a get call to {string}")
    public void iMakeAGetCallTo(String endPoint) {
        response = request.when().get(endPoint);
    }

    @And("I verify conversion for {string} {string}")
    public void iVerifyConversionFor(String sellCurrency, String buyCurrency) {
        if (sellCurrency.equalsIgnoreCase("GBP") && buyCurrency.equalsIgnoreCase("USD")) {
            Assert.assertEquals(amount, response.body().jsonPath().get("client_sell_amount"));
            double buyAmount = Double.parseDouble(response.body().jsonPath().get("client_buy_amount"));
            double clientRate = Double.parseDouble(response.body().jsonPath().get("client_rate"));
            double sellAmount = Double.parseDouble(amount);
            Assert.assertEquals(buyAmount, sellAmount * clientRate);
        } else if (sellCurrency.equalsIgnoreCase("EUR") && buyCurrency.equalsIgnoreCase("AUD")) {
            Assert.assertEquals(response.statusCode(), 400);
            Assert.assertEquals(response.body().jsonPath().getString("error_messages.amount[0].message"),
                    "amount should be of numeric type");
            Assert.assertEquals(response.body().jsonPath().getString("error_messages.amount[1].message"),
                    "amount can not be smaller than 1");
        }
    }
}

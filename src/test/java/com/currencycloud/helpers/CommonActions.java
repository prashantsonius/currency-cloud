package com.currencycloud.helpers;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class CommonActions {
    protected static Response response;
    protected static RequestSpecification request;
    protected static String authToken;
    protected static String amount;
    protected static Properties prop = new Properties();

    public static void loadProperties(Properties properties) throws IOException {
        FileInputStream file = new FileInputStream("environments.properties");
        properties.load(file);
        file.close();
    }
}

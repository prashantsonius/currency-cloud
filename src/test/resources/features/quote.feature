@currencyCould
Feature: get auth token and perform quote

  Background: login to get auth token
    Given I set login credential
    When I make call to get auth token
    Then I check status code "200"
    And I get auth token

  @quote
  Scenario Outline: create quote for given currency
    Given I set "<fixed_side>" "<amount>" "<sell>" to "<buy>"
    When I make a get call to "/rates/detailed"
    And I check status code "<statusCode>"
    Then I verify conversion for "<sell>" "<buy>"
    Examples:
      | fixed_side | amount   | sell | buy | statusCode |
      | sell       | 1500.00  | GBP  | USD | 200        |
      | sell       | -1000.00 | EUR  | AUD | 400        |


